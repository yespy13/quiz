// Declaración variables

var puntosCine;
var puntosDeportes;
var puntosMusic;
var puntosInfor;
var puntosTotales;
var nombre = "";
var tema;
var preguntasContestadasCine;
var preguntasContestadasDeportes;
var preguntasContestadasMusic;
var preguntasContestadasInfor;
var preguntasContestadas;
var respuestaCorrecta;
var opcionElegida;
var preguntasCine = new Array();
var preguntasMusic = new Array();
var preguntasDeportes = new Array();
var preguntasInfor = new Array();
var rellenarPregunta = document.getElementById("question");
var numeroDePreguntas = document.getElementById("preguntasContestadas");  
const pantallaOnLoad = document.getElementById("pantallaOnLoad");
const pantallaInicio = document.getElementById("pantallaInicio");
const pantallaRuleta = document.getElementById("pantallaRuleta");
const botonJugar = document.getElementById("botonJugar");
const pantallaPreguntas = document.getElementById("pantallaPreguntas");
const pantallaFinal = document.getElementById("pantallaFinal");
const botonAbandonar = document.getElementById("abandonar");
const seccionHeader = document.getElementById("header");
const seccionNav = document.getElementById("nav");
const seccionFooter = document.getElementById("footer");
const textoInfo = document.getElementById("textoInfo");
const botonOp1 = document.getElementById("op1");
const botonOp2 = document.getElementById("op2");
const botonOp3 = document.getElementById("op3");
const botonOp4 = document.getElementById("op4");
const botonValidar = document.getElementById("botonValidar");
const puntuacion = document.getElementById("puntuacion");
/*const listaPuntosCine = document.getElementById("puntosCine");
const listaPuntosDeportes = document.getElementById("puntosDeportes");
const listaPuntosMusic = document.getElementById("puntosMusic");
const listaPuntosInfor = document.getElementById("puntosInfor");
const listaPreguntasCine = document.getElementById("preguntasCine");
const listaPreguntasDeportes = document.getElementById("preguntasDeportes");
const listaPreguntasMusic = document.getElementById("preguntasMusic");
const listaPreguntasInfor = document.getElementById("preguntasInfor");*/
const puntuacionFinal = document.getElementById("puntuacionFinal");
const textoFinal = document.getElementById("mensajeFinal");
var textoVictoria = "";
textoDerrota = "";
textoDerrotaCine = "";
textoDerrotaDeportes = "";
textoDerrotaMusic = "";
textoDerrotaInfor = "";

// Inicialización

function inicializar() {
    console.log("funciona");
    textoInfo.style.display = "none";
    seccionHeader.style.display = "auto";
    puntuacion.innerHTML = "Puntos : 0";
    numeroDePreguntas.innerHTML = "Preguntas : 0/20";

    inicializarVariables();
    inicializarPreguntasCine();
    inicializarPreguntasDeportes();
    inicializarPreguntasMusic();
    inicializarPreguntasInfor();
}

function inicializarVariables() {
    puntosCine = 0;
    puntosDeportes = 0;
    puntosMusic = 0;
    puntosInfor = 0;
    preguntasContestadasCine = 0;
    preguntasContestadasDeportes = 0;
    preguntasContestadasMusic = 0;
    preguntasContestadasInfor = 0;
    preguntasContestadas = 0;
    respuestaCorrecta = 0;
    opcionElegida = 0;
    tema = "";
}

function inicializarPreguntasCine() {
    preguntasCine = [
        {
            pregunta: "<img src='img/alexdelaiglesia.jpg' id='imgPreguntas'><br />¿Quién es este director de cine, famoso por películas como El Día de la Bestia y Las Brujas de Zugarramurdi?",
            opciones: ["Julián López", "Alex de la Iglesia", "Enrique Urbizu", "Borja Cobeaga"],
            respuesta: "2"
        },
        {
            pregunta: "<img src='img/jupiter.png' id='imgPreguntas'><br />¿A qué película de las hermanas Wachowski corresponde esta imagen?",
            opciones: ["Jupiter Ascending", "The Matrix", "V for Vendetta", "Cloud Atlas"],
            respuesta: "1"
        },
        {
            pregunta: "<img src='img/dragonstone.jpg' id='imgPreguntas'><br />¿En qué lugar de Euskal Herria se puso el casillo de Dragonstone en Game of Thrones?",
            opciones: ["Castillo de Butrón", "Hiru Errege Mahaia", "Zarautz", "San Juan de Gaztelugatxe"],
            respuesta: "4"
        },
        {
            pregunta: "<img src='img/007.jpg' id='imgPreguntas'><br />¿Qué interprete del agente secreto 007 rodó una de sus películas en Bilbao?",
            opciones: ["Sean Connery", "Pierce Brosnan", "Daniel Craig", "Tom Hanks"],
            respuesta: "2"
        },
        {
            pregunta: "<img src='img/Hitchcock.jpeg' id='imgPreguntas'><br />¿Cuál de estas preguntas no es de Alfred Hitchcock?",
            opciones: ["39 escalones", "La soga", "The girl", "De entre los muertos"],
            respuesta: "3"
        },
        {
            pregunta: "<img src='img/Batman.jpeg' id='imgPreguntas'><br />¿Qué superpoder tiene Batman?",
            opciones: ["Convertirse en murciélago", "Ninguno", "Oído supersónico", "Súper fuerza"],
            respuesta: "4"
        }
    ]
}

function inicializarPreguntasDeportes() {
    preguntasDeportes = [
        {
            pregunta: "<img src='img/quidditch.jpg' id='imgPreguntas'><br />¿Cómo se llama el equipo de quidditch de Bizkaia?",
            opciones: ["Bizkaia Dragons", "Bilbao Leoiak", "Bizkaia Boggarts", "Bizkaia Basurdeak"],
            respuesta: "3"
        },
        {
            pregunta: "<img src='img/euskalkopabasket.jpg' id='imgPreguntas'><br />¿Cuál es el actual campeón de la Euskal Kopa de baloncesto?",
            opciones: ["Donostia Gipuzkoa Basket", "Araberri BC", "Bilbao Basket", "CD Saski Baskonia"],
            respuesta: "4"
        },
        {
            pregunta: "<img src='img/rugby.jpg' id='imgPreguntas'><br />¿Cuál es el equipo de rugby más exitoso de Euskal Herria?",
            opciones: ["Getxo Rugby", "Atlético San Sebastián", "Aviron Bayonnais", "Hernani CRE"],
            respuesta: "3"
        },
        {
            pregunta: "<img src='img/portland.jpg' id='imgPreguntas'><br />¿De dónde era el Portland San Antonio, campeón de Europa en 2001?",
            opciones: ["Tutera", "Atarrabia", "Iruña", "Otxandio"],
            respuesta: "3"
        },
        {
            pregunta: "<img src='img/bilbaorugbychampions.jpg' id='imgPreguntas'><br />¿Qué equipo se proclamó campeón de Europa en las finales de la Champions Cup de rugby que se disputó en San Mamés en 2018?",
            opciones: ["Racing 92", "Leinster Rugby", "Cardiff Blues", "Gloucester Rugby"],
            respuesta: "2"
        },
        {
            pregunta: "<img src='img/regionscup.jpg' id='imgPreguntas'><br />¿Cuántas veces ha ganado la Euskal Selekzioa la UEFA Regions' Cup?",
            opciones: ["-   2   -", "-   1   -", "-   0   -", "-   4   -"],
            respuesta: "2"
        }
    ]
}

function inicializarPreguntasInfor() {
    preguntasInfor = [
        {
            pregunta: "¿Cuántos bits hay en un MegaByte?",
            opciones: [" 5.379.527 ", "6.842.387", "8.388.608", "1024"],
            respuesta: "3"
        },
        {
            pregunta: "¿Qué tipo de variable usarás para almacenar 2.87?",
            opciones: ["  Integer  ", "  Float  ", "  String  ", "  Char  "],
            respuesta: "2"
        },
        {
            pregunta: "<img src='img/if.jpg' id='imgPreguntas'><br />¿Qué indica esto en un diagrama de flujo?",
            opciones: ["Condición", "Proceso", "Inicio/Fin", "Entrada/Salida"],
            respuesta: "1"
        },
        {
            pregunta: "¿Cuál es el nombre de la declaración que se realizará en un switch si no se ejecutan ninguna de las opciones programadas?",
            opciones: ["  Case  ", "  Else  ", "  Defect  ", "  Default  "],
            respuesta: "4"
        },
        {
            pregunta: "¿Qué es más óptimo usar cuando quieres repetir una serie de instrucciones hasta que una condición se cumpla, sin que haga falta que ocurra una vez?",
            opciones: ["  For  ", "  While  ", "  Do While  ", "  Repeat  "],
            respuesta: "2"
        },
        {
            pregunta: "¿Quién creó Linux?",
            opciones: ["Linux Torvalds", "Steve Jobs", "Linus Torvalds", "Andrew S. Tanenbaum"],
            respuesta: "3"
        }
    ]
}

function inicializarPreguntasMusic() {
    preguntasMusic = [
        {
            pregunta: "<img src='img/huntza.jpg' id='imgPreguntas'><br />¿Con qué canción saltó a la fama el grupo Huntza?",
            opciones: ["Aldapan Gora", "Iñundik Iñoare", "Elurretan", "Lasai, Lasai"],
            respuesta: "1"
        },
        {
            pregunta: "<img src='img/ubago.jpg' id='imgPreguntas'><br />¿Quién es este intérprete gasteiztarra, famoso por la canción Sin Miedo a Nada?",
            opciones: ["Mikel Erentxun", "Fermin Muguruza", "Alex Ubago", "Natxo de Felipe"],
            respuesta: "3"
        },
        {
            pregunta: "<audio autoplay='autoplay' controls='controls'><source src='audio/lau-teilatu.mp3'></audio><br />¿Cómo se llama esta canción?",
            opciones: ["Txoriak Txori", "Euskal Herrian Euskaraz", "Lau Teilatu", "Txanpon Baten Truke"],
            respuesta: "3"
        },
        {
            pregunta: "<audio autoplay='autoplay' controls='controls'><source src='audio/aita-semeak.mp3'></audio><br />¿A qué grupo versionó Celtas Cortos a la hora de hacer este tema para el disco 'Introversiones'?",
            opciones: ["Itoiz", "Hertzainak", "Ken Zazpi", "Oskorri"],
            respuesta: "4"
        },
        {
            pregunta: "<img src='img/sutagar.JPG' id='imgPreguntas'><br />¿Cuál es este grupo, conocido por canciones como Jo Ta Ke y Mari?",
            opciones: ["Su Ta Gar", "Berri Txarrak", "SA", "Latzen"],
            respuesta: "1"
        },
        {
            pregunta: "<img src='img/triki.jpg' id='imgPreguntas'><br />¿Cuál es el nombre de este instrumento tradicional vasco?",
            opciones: ["Alboka", "Trikitixa", "Txalaparta", "Txistu"],
            respuesta: "2"
        }
    ]
}

function inicializarTextosFinales() {
    textoVictoria = "Felicidades, " + nombre + ", has ganado EL JUEGO.";
    textoDerrota = nombre + ", has respondido mal un total de 20 preguntas, por lo que HAS PERDIDO.";
    textoDerrotaCine = nombre + ", has llegado al máximo de preguntas que podías responder en la categoría CINE. Has perdido el juego.";
    textoDerrotaDeportes = nombre + ", has llegado al máximo de preguntas que podías responder en la categoría DEPORTES. Has perdido el juego.";
    textoDerrotaMusic = nombre + ", has llegado al máximo de preguntas que podías responder en la categoría MÚSICA. Has perdido el juego.";
    textoDerrotaInfor = nombre + ", has llegado al máximo de preguntas que podías responder en la categoría INFORMÁTICA. Has perdido el juego.";
}

/*function mostrarPantallaOnLoad() {
    pantallaOnLoad.style.display = "block";
    pantallaInicio.style.display = "none";
    pantallaRuleta.style.display = "none";
    pantallaPreguntas.style.display = "none";
    pantallaFinal.style.display = "none";
    botonAbandonar.style.display = "none";
    seccionHeader.style.display = "none";
    seccionNav.style.display = "none";
    seccionFooter.style.display = "none";
    setTimeout(() => { mostrarPantallaInicio(); }, 3000);

} */

// Pantalla inicio

function mostrarPantallaInicio() {
    inicializar();
    ocultarPantallas();
    pantallaInicio.style.display = "block";
    seccionHeader.style.display = "block";
}

function guardarNombre() {
    txtNombreJugador = document.getElementById("nombreJugador");
    if (txtNombreJugador.value === "") {
        alert("Debes introducir un nombre");
        return false;
    } else {
        nombre = txtNombreJugador.value;
        txtNombreJugador.value = "";
        inicializarTextosFinales();
        return true;
    }
    console.log("Nombre: " + nombre);
}

function mostrarAyuda() {
    console.log(textoInfo)
    if (textoInfo.style.display === "none") {
        textoInfo.style.display = "grid";
    } else {
        textoInfo.style.display = "none";
    }
}

function jugar() {
    var nombreIntroducido = guardarNombre();
    if (nombreIntroducido) {
        mostrarPantallaRuleta();
    }
}

// Pantalla ruleta

function mostrarPantallaRuleta() {
    ocultarPantallas();
    pantallaRuleta.style.display = "block";
    botonAbandonar.style.display = "flex";
    seccionNav.style.display = "flex";
    seccionFooter.style.display = "flex"
}

function elegir() {
    girarRuleta();
    elegirPregunta();
    setTimeout(() => { mostrarPantallaPreguntas(); }, 5000);
    setTimeout(() => { botonJugar.value = "Elegir pregunta"; }, 5000);
}

function girarRuleta() {
    const R=2000
    let rand = Math.random() * R;
    ruleta.style.transform = "rotate(" + (rand) + "deg)";
    return rand;
}

function elegirPregunta() {
    rand = girarRuleta();
    valor = rand / 360;
    valor = (valor - parseInt(valor.toString().split(".")[0])) * 360;
    switch (true) {
        case valor <= 90:
            if (puntosCine < 3 ){
                rellenarPregunta.innerHTML=preguntasCine[preguntasContestadasCine].pregunta;
                respuestaCorrecta = preguntasCine[preguntasContestadasCine].respuesta;
                position=preguntasContestadasCine;
                preguntasContestadasCine++;
                tema = preguntasCine;
            } else {
                elegirPregunta();
            }
            break;
        case valor <= 180:
            if (puntosDeportes < 3 ){    
                rellenarPregunta.innerHTML=preguntasDeportes[preguntasContestadasDeportes].pregunta;
                respuestaCorrecta = preguntasDeportes[preguntasContestadasDeportes].respuesta;
                position=preguntasContestadasDeportes;
                preguntasContestadasDeportes++;
                tema = preguntasDeportes;
            } else {
            elegirPregunta();
            }
            break;
        case valor <= 270:
            if (puntosInfor < 3 ){  
                rellenarPregunta.innerHTML=preguntasInfor[preguntasContestadasInfor].pregunta;
                respuestaCorrecta = preguntasInfor[preguntasContestadasInfor].respuesta;
                position=preguntasContestadasInfor;
                preguntasContestadasInfor++;
                tema = preguntasInfor;
            } else {
                elegirPregunta();
            }
            break; 
        case valor <= 360:
            if (puntosMusic < 3 ){  
                rellenarPregunta.innerHTML=preguntasMusic[preguntasContestadasMusic].pregunta;
                respuestaCorrecta = preguntasMusic[preguntasContestadasMusic].respuesta;
                position=preguntasContestadasMusic;
                preguntasContestadasMusic++;
                tema = preguntasMusic;
            } else {
                elegirPregunta();
            }    
            break;
    }
    document.getElementById("op1").innerHTML = '<p class="choice-text" data-number="1">' + tema[position].opciones[0] + '</p>';
    document.getElementById("op2").innerHTML = '<p class="choice-text" data-number="2">' + tema[position].opciones[1] + '</p>';
    document.getElementById("op3").innerHTML = '<p class="choice-text" data-number="3">' + tema[position].opciones[2] + '</p>';
    document.getElementById("op4").innerHTML = '<p class="choice-text" data-number="4">' + tema[position].opciones[3] + '</p>';
}

// Pantalla preguntas

function mostrarPantallaPreguntas() {
    ocultarPantallas();
    seccionNav.style.display = "flex";
    pantallaPreguntas.style.display = "block";
    seccionFooter.style.display = "flex";
    botonValidar.value = "Validar";
    botonAbandonar.style.display = "flex";
    botonValidar.setAttribute("onclick", "validarRespuesta()");
}

function elegirRespuesta(op) {
    botonOp1.style.backgroundColor = (op === 1 ? "#cccc00" : "white");
    botonOp2.style.backgroundColor = (op === 2 ? "#cccc00" : "white");
    botonOp3.style.backgroundColor = (op === 3 ? "#cccc00" : "white");
    botonOp4.style.backgroundColor = (op === 4 ? "#cccc00" : "white");
    botonValidar.style.display = "flex";
    opcionElegida = op;
}

function validarRespuesta() {
    if(opcionElegida == respuestaCorrecta) {
        alert("¡Correcto!");
        botonOp1.style.backgroundColor = (opcionElegida === 1 ? "green" : "white");
        botonOp2.style.backgroundColor = (opcionElegida === 2 ? "green" : "white");
        botonOp3.style.backgroundColor = (opcionElegida === 3 ? "green" : "white");
        botonOp4.style.backgroundColor = (opcionElegida === 4 ? "green" : "white");
        darPuntos();
    } else {
        alert("Fail...");
        if (opcionElegida == 1) {
            botonOp1.style.backgroundColor = "red"
        } else if (opcionElegida == 2) {
            botonOp2.style.backgroundColor = "red"
        } else if (opcionElegida == 3) {
            botonOp3.style.backgroundColor = "red"
        } else if (opcionElegida == 4) {
            botonOp4.style.backgroundColor = "red"
        }

        if (respuestaCorrecta == 1) {
            botonOp1.style.backgroundColor = "green"
        } else if (respuestaCorrecta == 2) {
            botonOp2.style.backgroundColor = "green"
        } else if (respuestaCorrecta == 3) {
            botonOp3.style.backgroundColor = "green"
        } else if (respuestaCorrecta == 4) {
            botonOp4.style.backgroundColor = "green"
        }
    }

    preguntasContestadas = preguntasContestadasCine + preguntasContestadasDeportes + preguntasContestadasInfor + preguntasContestadasMusic;
    numeroDePreguntas.innerHTML=preguntasContestadas+"/20"
    /*listaPreguntasCine.innerHTML = "Cine: " + preguntasContestadasCine + " preguntas";
    listaPreguntasDeportes.innerHTML = "Deportes: " + preguntasContestadasDeportes + " preguntas";
    listaPreguntasMusic.innerHTML = "Música: " + preguntasContestadasMusic + " preguntas";
    listaPreguntasInfor.innerHTML = "Informática: " + preguntasContestadasInfor + " preguntas";*/

    botonValidar.value = "Continuar";
    botonValidar.setAttribute("onclick", "siguientePregunta()");
}

function darPuntos() {
    switch(tema) {
        case preguntasCine:
            puntosCine++;
            break;
        case preguntasDeportes:
            puntosDeportes++;
            break;
        case preguntasInfor:
            puntosInfor++;
            break;
        case preguntasMusic:
            puntosMusic++;
            break;
        default:
            alert("Fallo al sumar puntos");
    }

    puntosTotales = puntosCine + puntosDeportes + puntosMusic + puntosInfor;

    puntuacion.innerHTML = puntosTotales + " puntos";
    /*listaPuntosCine.innerHTML = "Cine: " + puntosCine + " puntos";
    listaPuntosDeportes.innerHTML = "Deportes: " + puntosDeportes + " puntos";
    listaPuntosMusic.innerHTML = "Música: " + puntosMusic + " puntos";
    listaPuntosInfor.innerHTML = "Informática: " + puntosInfor + " puntos";*/
}

function siguientePregunta() {
    var consecuencia = checkScore();
    siguientePaso(consecuencia);
    repintarCajas();
}

function siguientePaso(consecuencia) {
    if(consecuencia === "gana") {
        mostrarPantallaFinal(textoVictoria);
    } else if(consecuencia === "pierde") {
        mostrarPantallaFinal(textoDerrota);
        
    } else if(consecuencia === "pierdeCine") {
        mostrarPantallaFinal(textoDerrotaCine);
        
    } else if(consecuencia === "pierdeDeportes") {
        mostrarPantallaFinal(textoDerrotaDeportes);
        
    } else if(consecuencia === "pierdeMusic") {
        mostrarPantallaFinal(textoDerrotaMusic);
        
    } else if(consecuencia === "pierdeInfor") {
        mostrarPantallaFinal(textoDerrotaInfor);
    } else {
        mostrarPantallaRuleta();
    }
}

function repintarCajas() {
    botonOp1.style.backgroundColor = "white"
    botonOp2.style.backgroundColor = "white"
    botonOp3.style.backgroundColor = "white"
    botonOp4.style.backgroundColor = "white"
}

function checkScore() {
    var consecuencia = "";

    if (tema === preguntasInfor && puntosInfor === 3) {
        alert("¡Has completado las preguntas de Informática!");
    } else if(preguntasContestadasInfor === 5) {
        consecuencia = "pierdeInfor";
    }

    if (tema === preguntasMusic && puntosMusic === 3) {
        alert("¡Has completado las preguntas de Música!");
    } else if(preguntasContestadasMusic === 5) {
        consecuencia = "pierdeMusic";
    }

    if (tema === preguntasDeportes && puntosDeportes === 3) {
        alert("¡Has completado las preguntas de Deportes!");
    } else if(preguntasContestadasDeportes === 5) {
        consecuencia = "pierdeDeportes";
    }

    if (tema === preguntasCine && puntosCine == 3) {
        alert("¡Has completado las preguntas de Cine!");
    } else if(preguntasContestadasCine === 5) {
        consecuencia = "pierdeCine";
    }

    if (puntosTotales === 12) {
        consecuencia = "gana";
    } else if (preguntasContestadas === 20 && puntosTotales < 12) {
        consecuencia = "pierde";
    }

    return consecuencia;
}

// Pantalla final

function mostrarPantallaFinal(texto) {
    ocultarPantallas();
    pantallaFinal.style.display = "block";
    sacarTextos(texto);
}

function sacarTextos(texto) {
    puntuacionFinal.innerHTML = "Puntuación: " + puntosTotales;
    textoFinal.innerHTML = texto;
}

function jugarOtraVez() {
    inicializarVariables();
    puntuacion.innerHTML = "0 puntos";
    numeroDePreguntas.innerHTML = "0/20 preguntas";
    mostrarPantallaRuleta();
}

// Footer

function abandonar() {
    if (confirm("¿Estás seguro de que quieres abandonar?")) {
        mostrarPantallaInicio();
    }
}

// Otras funciones

function ocultarPantallas() {
    pantallaFinal.style.display = "none";
    pantallaInicio.style.display = "none";
    pantallaRuleta.style.display = "none";
    pantallaPreguntas.style.display = "none";
    botonAbandonar.style.display = "none";
    botonValidar.style.display = "none";
    seccionHeader.style.display = "none";
    seccionNav.style.display = "none";
    seccionFooter.style.display = "none";
}